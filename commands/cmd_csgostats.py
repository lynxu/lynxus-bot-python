import discord
import requests
from bs4 import BeautifulSoup

def ex(args,message,client,invoke):
    if len(args) > 0:
        namedisc = args.__str__()[1:-1].replace(",", "").replace("'", "")
        if "https://steamcommunity.com/id/" in str(namedisc):
            namedisc=namedisc[30:-1]
        elif "http://steamcommunity.com/id/" in str(namedisc):
            namedisc = namedisc[29:]
        elif "http://steamcommunity.com/profiles/" in str(namedisc):
            namedisc = namedisc[35:]
        elif "steamcommunity.com/id/" in str(namedisc):
            namedisc= namedisc[22:]
        elif "steamcommunity.com/profiles/" in str(namedisc):
            namedisc= namedisc[28:]
        else: namedisc = namedisc
        urlcsgo = "https://csgo-stats.net/player/%s/" % namedisc
        r = requests.get(urlcsgo)
        soup = BeautifulSoup(r.content, "html.parser")
        n_data = soup.find_all("h1")
        img_data = soup.find_all("div", {"class": "pv-main-custom"})
        player_data = soup.find_all("h2", {"class": "m-0 c-white f-300"})
        if img_data == [] and player_data == []:
            yield from client.send_message(message.channel, embed=discord.Embed(color=discord.Color.red(), description="This profile is `unknown`."))
        else:
            name = n_data[0].text
            avatar = img_data[0].get("style")[21:-2]
            kills = player_data[0].text
            deads = player_data[1].text
            hours_played = player_data[2].text
            mvps = player_data[3].text
            accuracy = player_data[4].text
            headshots_ratio = player_data[5].text
            win_ratio = player_data[6].text
            kd_data = float(kills)/float(deads)
            kd_data = round(kd_data,2)

            em = discord.Embed(color=discord.Color.green(), title=(":open_file_folder:  Counter Strike: Global Offensive Profile: %s" % (name)),description="You asked for it, you got it:")
            em.set_author(
                name="Lynxu's BOT - Specially designed for anyone",
                icon_url=client.user.avatar_url,
                url="http://steamcommunity.com/id/lynxu"
            )
            em.add_field(
                name="Kills:",
                value=kills,
                inline=True
            )
            em.add_field(
                name="Deaths:",
                value=deads,
                inline=True
            )
            em.add_field(
                name="K/D:",
                value=str(kd_data),
                inline=True
            )
            em.add_field(
                name="Hours Played:",
                value=hours_played,
                inline=True
            )
            em.add_field(
                name="MVP's:",
                value=mvps,
                inline=True
            )
            em.add_field(
                name="Accuracy:",
                value=accuracy,
                inline=True
            )
            em.add_field(
                name="Headshot Ratio:",
                value=headshots_ratio,
                inline=True
            )
            em.add_field(
                name="Win Ratio:",
                value=win_ratio,
                inline=True
            )
            em.set_footer(
                text="Bot made by Lynxu.",
                icon_url="http://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/4e/4e418f795f52d898e0a31319549cbebaeeced3ad_full.jpg"
            )
            em.set_thumbnail(
                url=avatar
            )
            yield from client.send_message(message.channel, embed=em)
    elif len(args)>1:
        yield from client.send_message(message.channel,embed=discord.Embed(color=discord.Color.red(), description="This command can't show `multiple stats` at a time!"))
    else:
        yield from client.send_message(message.channel, embed=discord.Embed(color=discord.Color.red(),description="This command needs `steamID64`, `steamCustomURL` or the `link` to the profile."))