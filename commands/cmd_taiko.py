import discord
import requests
from discord.ext import commands
from bs4 import BeautifulSoup




def ex(args,message,client,invoke):
    if len(args)>0:
        namedisc = args.__str__()[1:-1].replace(",", "").replace("'", "")
        #print(namedisc)
        urlrank = "https://syrin.me/osuchan/u/%s/?m=1" % (namedisc)
        r = requests.get(urlrank)
        soup = BeautifulSoup(r.content, "html.parser")
        u_data = soup.find_all("h3")
        #print(u_data)
        #rank_worldwide = u_data[1].text
        n_data = soup.find_all("h1")
        #print(n_data)
        #name = n_data[0].text
        pp_data = soup.find_all("h5")
        #print(pp_data)
        #performance_points = pp_data[0].text
        lvl_data = soup.find_all("div", {"style": "float: left; margin-right: 5px"})
        #level = lvl_data[0].text
        #print(lvl_data)
        acc_data = soup.find_all("td")
        #print(acc_data)
        #play_count = acc_data[6].text
        #accuracy = acc_data[8].text
        country_data = soup.find_all("img")
        #print(country_data)
        #country = country_data[8].get('title', '')
        rankcountry_data = soup.find_all('h4')
        #print(rankcountry_data)
        #rank_country = rankcountry_data[2].text
        img_data = soup.find_all("img")
        if pp_data==[] and lvl_data==[] and acc_data == []:
            name = n_data[0].text
            yield from client.send_message(message.channel, embed=discord.Embed(color=discord.Color.red(), description="`%s` has't played this mode yet!" % (name)))
        elif n_data == [] and lvl_data == [] and acc_data == []:
            performance_points = pp_data[0].text
            yield from client.send_message(message.channel, embed=discord.Embed(color=discord.Color.red(),description=("%s" % performance_points)))
        else:
            rank_worldwide = u_data[1].text
            name = n_data[0].text
            performance_points = pp_data[0].text
            level = lvl_data[0].text
            play_count = acc_data[6].text
            accuracy = acc_data[8].text
            country = country_data[8].get('title', '')
            rank_country = rankcountry_data[2].text
            avatar = img_data[7].get('src', '')
            em = discord.Embed(color=discord.Color.green(), title=":open_file_folder:  Osu! Profile: %s" % name , description="You asked for it, you got it:")
            em.set_author(
                name = "Lynxu's BOT - Specially designed for anyone",
                icon_url =client.user.avatar_url,
                url = "http://steamcommunity.com/id/lynxu"
            )
            em.add_field(
                name = "Level:",
                value=level,
                inline=True
            )
            em.add_field(
                name = "Performance Points:",
                value = performance_points,
                inline= True
            )
            em.add_field(
                name = "Worldwide Rank:",
                value=rank_worldwide,
                inline=True
            )
            em.add_field(
                name = "Rank Country:",
                value= rank_country + (" :flag_%s" % country.lower()) + ":",
                inline=True
            )
            em.add_field(
                name = "Accuracy:",
                value= accuracy,
                inline= True
            )
            em.add_field(
                name = "Play count:",
                value= play_count,
                inline=True
            )
            em.set_footer(
                text = "Bot made by Lynxu.",
                icon_url= "http://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/4e/4e418f795f52d898e0a31319549cbebaeeced3ad_full.jpg"
            )
            em.set_thumbnail(
                url=avatar
            )
            yield from client.send_message(message.channel, embed=em)
            #yield from client.send_message(message.channel, embed=discord.Embed(color=discord.Color.green(),description=("`%s`\n**Level:** `%s`\n**PP:** `%s`\n**Worldwide Rank:** `%s`\n**Country Rank:** `%s` :flag_%s:\n**Accuracy:** `%s`\n**Play Count:** `%s`\n" %(name,level,performance_points,rank_worldwide,rank_country,country.lower(),accuracy,play_count))))
    else:
        yield from client.send_message(message.channel, embed=discord.Embed(color=discord.Color.red(), description="You need to give a **username** for this command!"))