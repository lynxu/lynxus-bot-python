import os
from os import path
import discord

def error(content,channel,client):
    yield from client.send_message(channel,embed=discord.Embed(color=discord.Color.red(),description=content))

def get(server):
    f = "ROLESETTINGS/" + server.id + "/autorole"
    if path.isfile(f):
        with open(f) as f:
            return discord.utils.get(server.roles, id=f.read())
    else:
        return None

def save_file(id,server):
    if not path.isdir("ROLESETTINGS/" + server.id):
        os.makedirs("ROLESETTINGS/" + server.id)
    with open("ROLESETTINGS/" + server.id + "/autorole","w") as f:
        f.write(id)
        f.close()


def ex(args, message, client, invoke):
    print (args)
    if message.author == message.server.owner:
        if len(args)>0:
            rolename = args.__str__()[1:-1].replace(",","").replace("'","")
            print (rolename)
            role = discord.utils.get(message.server.roles,name=rolename)
            if role == None:
                yield from error("Enter a valid role existing on the server!", message.channel,client)
            else:
                try:
                    save_file(role.id,message.server)
                    yield from client.send_message(message.channel, embed=discord.Embed(color=discord.Color.green(),description=("Successfully set the role to `%s`" % role.name)))
                except Exception:
                    yield from error("Something went wrong while saving roles!",message.channel,client)
                    raise Exception
    else:
        yield from client.send_message(message.channel, embed=discord.Embed(color=discord.Color.red(),description=("Only the **Owner** of the server can use this command!")))