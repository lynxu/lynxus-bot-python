import discord
import asyncio as asyncio
import os
import SECRETS
import STATICS
from discord import Game, Server, Member, Embed, Color
from discord.ext import commands
from commands import cmd_ping, cmd_autorole, cmd_osu, cmd_ctb,cmd_mania,cmd_taiko,cmd_csgostats,cmd_csgoinventory

print (discord.version_info)
print(discord.__version__)
print(os.getcwd())

description ='Lynxu Bot'
client = discord.Client()
commands = {
    "ping": cmd_ping,
    "autorole": cmd_autorole,
    "osu": cmd_osu,
    "ctb": cmd_ctb,
    "taiko": cmd_taiko,
    "mania": cmd_mania,
    "csgostats": cmd_csgostats,
    "csgoinventory": cmd_csgoinventory,
}


@client.event
@asyncio.coroutine
async def on_ready():
    print("Logged in")
    print('Name:{}'.format(client.user.name))
    print('ID:{}'.format(client.user.id))
    print("Bot is running successfully. Running on servers:\n")
    for s in client.servers:
        print("- %s (%s)" %(s.name,s.id))
        await client.change_presence(game=Game(name="$help for more info"))


@client.event
@asyncio.coroutine
def on_message(message):
    if message.content.startswith(STATICS.PREFIX):
        invoke = message.content[len(STATICS.PREFIX):].split(" ")[0]
        args = message.content.split(" ")[1:]
        #print (invoke)
        #print(args)
        #print("INVOKE: %s\nARGS: %s" % (invoke,args.__str__()[1:-1].replace("'", "")))
        if commands.__contains__(invoke):
            yield from commands.get(invoke).ex(args, message, client, invoke)
        else:
            yield from client.send_message(message.channel, embed=Embed(color=discord.Color.red(),description=("The command `%s` is not valid!" % invoke)))
            # message.author trimite mesaj pe

@client.event
@asyncio.coroutine
def on_member_join(member):
    yield from client.send_message(member, embed=Embed(color=discord.Color.green(),description=("**Hello %s!**\n Welcome to *%s* owned by %s.\n Make sure you make lots of new friends who share the same culture as you. We are all a man of a culure.\n Now have a nice day" %(member.name,member.server.name,member.server.owner.mention))))
    role = cmd_autorole.get(member.server)
    if role != None:
        yield from client.add_roles(member,role)
        try:
            yield from client.send_message(member,"Well you just got assigned with the role " + role.mention + "!\nBut you can always climb your way up.")
        except Exception:
            yield from client.send_message(member,"Sorry, but I don't have permission to automatically assign you the role " + role.mention + ".")
            raise Exception
client.run(SECRETS.TOKEN)
