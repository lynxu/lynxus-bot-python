import requests
from bs4 import BeautifulSoup

urlcsgo = "https://csgo-stats.net/player/lynxu/"
r = requests.get(urlcsgo)
soup = BeautifulSoup(r.content,"html.parser")
n_data = soup.find_all("h1")
#print(n_data)
name = n_data[0].text
print(name)
img_data = soup.find_all("div", {"class": "pv-main-custom"})
#print(img_data)
img = img_data[0].get("style")[21:-2]
print(img)
player_data = soup.find_all("h2", {"class": "m-0 c-white f-300"})
#print(player_data)
kills = player_data[0].text
print(kills)
deads = player_data[1].text
print(deads)
hours_played = player_data[2].text
print(hours_played)
mvps = player_data[3].text
print(mvps)
accuracy = player_data[4].text
print(accuracy)
headshots_ratio = player_data[5].text
print(headshots_ratio)
win_ratio = player_data[6].text
print(win_ratio)
items = player_data[7].text
print(items)
items_value = player_data[8].text
print(items_value)
#kd_data = soup.find_all("div", {"id": "kdRatio", "style":"margin: 23px auto;"})
kd_data = int(kills)/int(deads)
kd_data = round(kd_data,2)
print(kd_data)